import "./App.css";
import { Fragment, useCallback, useEffect } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { listActions } from "./store/list";
import { useSelector } from "react-redux";
import ListItem from "./components/ListItem";
import AddListItem from "./components/AddListItem";

function App() {
  // For redux
  let dispatch = useDispatch();

  // For making requests to firebase
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  // Establish initial connection to database to get initial list
  const fetchTodoListHandler = useCallback(async () => {
    setIsLoading(true);
    setError(null);

    try {
      const response = await fetch(
        "https://to-do-a5ac3-default-rtdb.firebaseio.com/todo.json"
      );

      if (!response.ok) {
        throw new Error("Something went wrong.");
      }

      const data = await response.json();

      // This is a very poor implementation, I unfortunately don't actually know a better way so I am sorry. Everytime the list is saved, it makes a new object in the database rather than replacing the existing.

      // This gets the last object in the database' data and uses that to display the list
      let lastValue = data[Object.keys(data).pop()];

      const loadedTodoList = [];

      lastValue.forEach((listItem, index) => {
        loadedTodoList.push({
          id: index,
          text: listItem.text,
        });
      });

      dispatch(listActions.setList(loadedTodoList));
    } catch (error) {
      setError(error.message);
    }
    setIsLoading(false);
  }, [dispatch]);

  useEffect(() => {
    fetchTodoListHandler();
  }, [fetchTodoListHandler]);

  // Get the list of items
  let listOfItems = useSelector((state) => state.list.list);

  // Add a new item to the list
  const addItem = (itemToAdd) => {
    dispatch(listActions.addItem(itemToAdd));
  };

  // Removes an item from the list, uses the index of the item
  const removeItem = (itemToRemove) => {
    dispatch(listActions.removeItem(itemToRemove));
  };

  // Save an edit to an item, get an object with the index in the redux array + the text
  const editItemHandler = (itemToEdit) => {
    dispatch(listActions.editItem(itemToEdit));
  }

  // Render the list of items
  const renderList = () => {
    let returnString = null;
    returnString = listOfItems.map((listItem, index) => {
      return (
        <ListItem
          key={index}
          index={index}
          text={listItem.text}
          removeItem={removeItem}
          editItemHandler={editItemHandler}
        ></ListItem>
      );
    });

    return returnString;
  };

  // Submit list to firebase

  const saveListHandler = () => {
    // If the list of items is empty, and the user attemps to save. Submit with a placeholder string.
    if (listOfItems.length === 0) {
      listOfItems = [{ text: "Your first task..." }];
    }

    fetch("https://to-do-a5ac3-default-rtdb.firebaseio.com/todo.json", {
      method: "POST",
      body: JSON.stringify(listOfItems),
      header: {
        "Content-type": "application/json",
      },
    });
  };

  return (
    <Fragment>
      <div className="main-body" style={{ minHeight: window.innerHeight }}>
        {isLoading ? (
          <Fragment>
            <div className="loading-div">
              <h1>Loading...</h1>
              {error && <p>{error}</p>}
            </div>
          </Fragment>
        ) : (
          <Fragment>
            <div className="header-div">
              <h1>To-do List:</h1>
              <p>The Date Is: {new Date().toLocaleString() + ""}</p>
            </div>
            <div className="list-container">
              {/* For each item in the redux array, render a list item component */}
              {renderList()}
            </div>
            <div className="divider"></div>
            <div className="add-item-container">
              <AddListItem addItem={addItem} />
            </div>
            <div className="divider"></div>
            <div onClick={saveListHandler} className="save-list-container">
              <p>Save List</p>
            </div>
          </Fragment>
        )}
      </div>
    </Fragment>
  );
}

export default App;
