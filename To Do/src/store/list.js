import { createSlice } from "@reduxjs/toolkit";

const initialListState = {
  list: [],
};

const listSlice = createSlice({
  name: "list",
  initialState: initialListState,
  reducers: {
    setList(state, action) {
      state.list = action.payload;
    },

    addItem(state, action) {
      // Add new item to redux array by appending it to end
      state.list = [...state.list, action.payload];
    },

    removeItem(state, action) {
      // Remove an item from the list, one that matches the index given by App js
      state.list = state.list.filter(
        (listItem, index) => action.payload !== index
      );
    },

    editItem(state, action) {
      // Modify the item by mapping the array. If matches the index of the item being added, return a differing text
      state.list = state.list.map((listItem, index) => {
        if (index === action.payload.index) {
          console.log("jey");
          return {...listItem, text: action.payload.text}
        };
        return listItem;
      });
    },
  },
});

export const listActions = listSlice.actions;

export default listSlice.reducer;
