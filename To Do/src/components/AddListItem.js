import { useState } from "react";
import style from "./AddListItem.module.css";

const AddListItem = (props) => {
  // User input
  const [userInput, setUserInput] = useState("");

  // When button pressed, add item to the redux array (only if not blank)
  const addListItem = (event) => {
    event.preventDefault();

    if (userInput !== "") {
      // Submit to redux array through parent
      props.addItem({ text: userInput });

      // Clear user input
      setUserInput("");
    }
  };

  // Change state of input when input entered
  const userInputHandler = (event) => {
    setUserInput(event.target.value);
  };

  return (
    <div>
      <form onSubmit={addListItem} className={style.itemForm}>
        <input
          type="text"
          name="text"
          onInput={userInputHandler}
          value={userInput}
          placeholder="Add new task..."
        />
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
};

export default AddListItem;
