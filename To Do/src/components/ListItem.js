import style from "./ListItem.module.css";
import { useState } from "react";

const ListItem = (props) => {
  // This is the edit panel, hide when its not needed
  const [showEdit, setShowEdit] = useState(false);
  const [editInput, setEditInput] = useState(props.text)

  // Call parent to remove item from array
  const removeItemHandler = () => {
    props.removeItem(props.index);
  };

  const showEditHandler = () => {
    setShowEdit(!showEdit);
  }

  // Change state of input when input entered
  const editInputHandler = (event) => {
    setEditInput(event.target.value);
  };

  const editCompleteHandler = (event) => {
    event.preventDefault();
    setShowEdit(false);
    props.editItemHandler({index: props.index, text: editInput});
  }

  return (
    <div className={style.relativeContainer}>
      {showEdit && (
        <div className={style.editDiv}>
          <form className={style.editForm} onSubmit={editCompleteHandler}>
            <input type="text" value={editInput} onInput={editInputHandler}></input>
            <input type="submit" value="Save" />
            <input type="button" onClick={showEditHandler} value="Close"></input>
          </form>
        </div>
      )}

      <div className={style.containerDiv}>
        <p className={style.taskText}>{props.text}</p>
        <div className={style.buttonContainer}>
          <div onClick={showEditHandler} className={showEdit ? style.removeButton : style.editButton}>
            {showEdit ? <p>Close</p> : <p>Edit</p>}
          </div>
          <div onClick={removeItemHandler} className={style.removeButton}>
            <p>Remove</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ListItem;
